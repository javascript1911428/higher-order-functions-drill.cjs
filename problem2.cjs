const data = require("./data.cjs");

function peopleByHouses(data) {
    let houses = data["houses"];
    const people = houses.reduce((accumulator, entry) => {
        accumulator[entry["name"]] = entry["people"].length;
        return accumulator;
    }, {})
    return people;
}

let result = peopleByHouses(data);
console.log(result);