const data = require("./data.cjs");

function peopleNameOfAllHouses(data) {
    let houses = data["houses"];
    const people = houses.reduce((accumulator, entry) => {
        let name = entry["name"];
        let members = entry["people"];
        accumulator[name] = members.map((item) => item["name"]);
        return accumulator;
        }, {})
    return people;
}

let result = peopleNameOfAllHouses(data);
console.log(result);