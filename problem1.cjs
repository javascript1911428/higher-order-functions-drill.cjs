const data = require("./data.cjs");

function countAllPeople(data) {
    let houses = data['houses'];
    let count = houses.reduce((accumulator, entry) => {
        accumulator += (entry["people"].length);
        return accumulator;
    }, 0)
    return count;
}

let result = countAllPeople(data);
console.log(result);