const data = require("./data.cjs");

function everyone(data) {
    let houses = data["houses"];
    let namesList = houses.reduce((accumulator, entry) => {
        let people = entry["people"];
        accumulator.push(...people.map((item) => item["name"]).filter((fullName) => {
            let lastName = fullName.split(" ")[1];
            return lastName && lastName.startsWith("S");
        }));
        return accumulator;
    }, [])
    return namesList;
}

let result = everyone(data);
console.log(result);